/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Root} from 'native-base';

import Navigator from './src/app/navigations';
import {StatusBar} from 'react-native';

const App = () => {
  return (
    <Root>
      <StatusBar barStyle={'dark-content'} />
      <Navigator />
    </Root>
  );
};

export default App;
