import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';

export default class NotificationStatus extends Component {
  render() {
    return <View style={styles.circleView} />;
  }
}

const styles = StyleSheet.create({
  circleView: {
    width: 8,
    height: 8,
    backgroundColor: '#bb271f',
    opacity: 1,
    borderRadius: 100 / 2,
  },
});
