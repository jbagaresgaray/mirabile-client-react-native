import React, {useState} from 'react';
import {StyleSheet, Platform, Dimensions} from 'react-native';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Button,
  Right,
  Text,
} from 'native-base';
import {SafeAreaView} from 'react-native-safe-area-context';
import {DatePicker} from 'react-native-wheel-pick';

import globals from '../../../../styles/Global';

const styles = StyleSheet.create({
  innerContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: '60%',
    backgroundColor: 'transparent',
  },
});

export default ({navigation}) => {
  const [curDate, setCurDate] = useState(new Date());
  const isIos = Platform.OS === 'ios';

  return (
    <Container style={styles.innerContainer}>
      <SafeAreaView style={globals.viewFlex}>
        <Header>
          <Left />
          <Body />
          <Right>
            <Button transparent onPress={() => navigation.goBack()}>
              <Text>Done</Text>
            </Button>
          </Right>
        </Header>
        <Content bounces={false} scrollEnabled={false}>
          <DatePicker
            style={{
              backgroundColor: 'white',
              height: 215,
              width: isIos ? Dimensions.get('window').width : undefined,
            }}
            // android not support width
            minimumDate={new Date('1900-01-01')}
            maximumDate={new Date('2999-12-31')}
            onDateChange={date => {
              setCurDate(date);
            }}
          />
        </Content>
      </SafeAreaView>
    </Container>
  );
};
