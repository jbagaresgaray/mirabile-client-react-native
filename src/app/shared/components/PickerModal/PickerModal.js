import React, {useState} from 'react';
import {StyleSheet, Platform, Dimensions} from 'react-native';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Button,
  Text,
} from 'native-base';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Picker} from 'react-native-wheel-pick';

import globals from '../../../../styles/Global';

const isIos = Platform.OS === 'ios';
const styles = StyleSheet.create({
  innerContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: '65%',
    backgroundColor: 'transparent',
    // height: 260,
  },
  pickerToolbar: {
    height: 44,
  },
  pickerToolbarButton: {marginBottom: 20},
  pickerView: {
    backgroundColor: 'white',
    width: Dimensions.get('window').width,
    height: 215,
  },
});

export default props => {
  const {pickerData} = props.route.params;

  return (
    <Container style={styles.innerContainer}>
      <SafeAreaView style={globals.viewFlex}>
        <Header style={styles.pickerToolbar}>
          <Left style={styles.pickerToolbarButton}>
            <Button transparent onPress={() => props.navigation.goBack()}>
              <Text>Cancel</Text>
            </Button>
          </Left>
          <Body />
          <Right style={styles.pickerToolbarButton}>
            <Button transparent onPress={() => props.navigation.goBack()}>
              <Text>Done</Text>
            </Button>
          </Right>
        </Header>
        <Content bounces={false} scrollEnabled={false}>
          <Picker
            style={styles.pickerView}
            selectedValue="item4"
            pickerData={pickerData}
            onValueChange={value => {}}
            itemSpace={30} // this only support in android
          />
        </Content>
      </SafeAreaView>
    </Container>
  );
};
