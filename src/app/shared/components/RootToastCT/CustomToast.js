import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import Toast from './Toast';
import Colors from '../../../../styles/Colors';
import {
  FONT_MEDIUM,
  FONT_BOLD,
  FONT_REGULAR,
} from '../../../../styles/Typography';
import {Icon} from 'native-base';

const styles = StyleSheet.create({
  toastView: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: '#000',
    borderRadius: 14,
    flex: 1,
    position: 'relative',
    zIndex: 1000,
  },
  textMessageView: {
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    paddingLeft: 26,
    paddingRight: 26,
    paddingBottom: 30,
    paddingTop: 15,
  },
  textIcon: {
    fontSize: 60,
    color: Colors.colorWhite,
    marginBottom: 20,
    ...FONT_BOLD,
  },
  textMessage: {
    color: Colors.colorWhite,
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'center',
    ...FONT_REGULAR,
  },
});

export default function CustomToast(props) {
  const toastConfig = {
    duration: Toast.durations.LONG,
    position: Toast.positions.CENTER,
    shadow: true,
    animation: true,
    delay: 0,
  };

  return (
    <Toast visible={props.visible} {...toastConfig} style={styles.toastView}>
      <View style={styles.textMessageView}>
        <Icon name={props.icon} type={props.iconType} style={styles.textIcon} />
        <Text style={styles.textMessage}>{props.message}</Text>
      </View>
    </Toast>
  );
}
