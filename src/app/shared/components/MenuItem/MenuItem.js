import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {ListItem, Left, Thumbnail, Body, Text} from 'native-base';
import {FONT_MEDIUM} from '../../../../styles/Typography';
import Colors from '../../../../styles/Colors';

const styles = StyleSheet.create({
  menuThumbnail: {
    width: 70,
    height: 70,
    borderRadius: 6,

    shadowColor: '#00000019',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  menuTitle: {
    fontSize: 16,
    lineHeight: 19,
    letterSpacing: 0,
    color: Colors.colorStandard,
    ...FONT_MEDIUM,
  },
});

export default class MenuItem extends Component {
  render() {
    return (
      <ListItem thumbnail>
        <Left>
          <Thumbnail
            style={styles.menuThumbnail}
            square
            source={this.props.image}
          />
        </Left>
        <Body>
          <Text style={styles.menuTitle}>{this.props.title}</Text>
          <Text note numberOfLines={1}>
            {this.props.type}
          </Text>
          <Text note numberOfLines={1}>
            {this.props.store_location}
          </Text>
        </Body>
      </ListItem>
    );
  }
}
