import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import {
  Content,
  Container,
  Header,
  Left,
  Button,
  Body,
  Title,
  Right,
  Icon,
} from 'native-base';
import {WebView} from 'react-native-webview';

import globals from '../../../../styles/Global';

export default class ContentModalScreen extends Component {
  constructor(props) {
    super(props);
    const {title, content, type} = this.props.route.params;

    this.state = {
      title: title,
      content: content,
      type: type,
    };
  }

  render() {
    console.log('this.props.route.params: ', this.props.route.params);
    console.log('this.state: ', this.state);

    const runFirst = `
      document.body.style.backgroundColor = 'red';
      setTimeout(function() { window.alert('hi') }, 2000);
      true; // note: this is required, or you'll sometimes get silent failures
    `;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" style={globals.appBackButton} />
            </Button>
          </Left>
          <Body>
            <Title>{this.state.title}</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          {this.state.type === 'web' && (
            <>
              <WebView
                style={styles.container}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{uri: this.state.content}}
                injectedJavaScript={runFirst}
              />
            </>
          )}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#e5e5e5',
  },
});
