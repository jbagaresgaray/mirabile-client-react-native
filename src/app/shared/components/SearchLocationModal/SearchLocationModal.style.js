const {StyleSheet} = require('react-native');
import Colors from '../../../../styles/Colors';
import {FONT_REGULAR} from '../../../../styles/Typography';

export default StyleSheet.create({
  innerContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: '20%',
  },
  searchBarHeader: {
    marginTop: -20,
  },
  searchBarContainer: {
    borderRadius: 6,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: Colors.colorSeparator,

    height: 36,
    backgroundColor: Colors.colorWhite,
  },
  searchBarText: {
    fontSize: 12,
    lineHeight: 14,
    letterSpacing: 0,
    color: Colors.colorSeparatorText,
    ...FONT_REGULAR,

    marginLeft: 11,
  },
});
