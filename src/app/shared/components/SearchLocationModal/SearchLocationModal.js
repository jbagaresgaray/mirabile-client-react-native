import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {
  Content,
  Container,
  Header,
  Title,
  Right,
  Body,
  Button,
  Left,
  Icon,
  Item,
  Input,
  List,
  ListItem,
} from 'native-base';

import globals from '../../../../styles/Global';
import styles from './SearchLocationModal.style';

export default class SearchLocationModalScreen extends Component {
  render() {
    return (
      <Container style={styles.innerContainer}>
        <Header transparent style={globals.appPaddingModal}>
          <Left style={globals.viewFlex}>
            <Title>Search Location</Title>
          </Left>
          <Body style={globals.viewFlex5} />
          <Right>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="close" style={globals.appBackButton} />
            </Button>
          </Right>
        </Header>
        <Header
          searchBar
          transparent
          style={[styles.searchBarHeader, globals.appPaddingModal]}>
          <Item style={styles.searchBarContainer}>
            <Input
              placeholder="Search for your location..."
              style={styles.searchBarText}
            />
          </Item>
        </Header>

        <Content>
          <List>
            <ListItem>
              <Text>Use current location</Text>
            </ListItem>
            <ListItem>
              <Text>Search by postal code</Text>
            </ListItem>
            <ListItem>
              <Text>Search by State</Text>
            </ListItem>
            <ListItem>
              <Text>Search by Address</Text>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
