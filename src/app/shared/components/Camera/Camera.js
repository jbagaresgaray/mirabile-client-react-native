import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  Content,
  Container,
  Header,
  Title,
  Right,
  Body,
  Button,
  Left,
} from 'native-base';
import {withNavigationFocus} from '@react-navigation/compat';
import {RNCamera} from 'react-native-camera';

class CameraScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hasCameraPermission: true,
    };
  }

  cameraView() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Text>Cancel</Text>
            </Button>
          </Left>
          <Body>
            <Title>Camera</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <RNCamera />
        </Content>
      </Container>
    );
  }

  render() {
    const {isFocused} = this.props;
    const {hasCameraPermission} = this.state;

    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else if (isFocused) {
      return this.cameraView();
    } else {
      return <View />;
    }
  }
}

export default withNavigationFocus(CameraScreen);
