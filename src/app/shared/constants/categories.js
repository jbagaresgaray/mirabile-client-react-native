export const CATEGORIES = [
  {
    id: 1,
    name: 'Appetizers',
    image: './assets/png/categories/appetizer.png',
  },
  {
    id: 2,
    name: 'Breakfast',
    image: './assets/png/categories/breakfast.png',
  },
  {
    id: 3,
    name: 'Dinner',
    image: './assets/png/categories/dinner.png',
  },
];
