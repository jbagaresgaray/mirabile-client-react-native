export const MENUS = [
  {
    id: 1,
    title: 'Wasai Japanese Kitchen',
    type: 'Japanese',
    store_status: 'Open Now',
    star_rating: 3.3,
    store_location: '4830 Christiansen Locks Suite 690',
    image: require('../../../assets/png/menus/menu1.png'),
    image_lg: require('../../../assets/png/menus/menu1.png'),
    image_xl: require('../../../assets/png/menus/menu1.png'),
    addons: [
      {
        name: 'Appetizers',
        items: [
          {
            title: 'Beef Tataki',
            description:
              'This slices of rare wagyu beef fillet serve with red onions, spring onions and garlic sauce',
            price: '13.00',
            image: require('../../../assets/png/menus/menu1.png'),
            image_lg: require('../../../assets/png/menus/menu1.png'),
            image_xl: require('../../../assets/png/menus/menu1.png'),
          },
          {
            title: 'Karaage',
            description:
              'Juicy bite size pieces of Japanese style deep fried chicken with a sweet vinegar sauce',
            price: '8.00',
            image: require('../../../assets/png/menus/menu2.png'),
            image_lg: require('../../../assets/png/menus/menu2.png'),
            image_xl: require('../../../assets/png/menus/menu2.png'),
          },
        ],
      },
      {
        name: 'Dessert',
        items: [
          {
            title: 'Yuzu Cheesecake',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '6.00',
            image: require('../../../assets/png/menus/menu1.png'),
            image_lg: require('../../../assets/png/menus/menu1.png'),
            image_xl: require('../../../assets/png/menus/menu1.png'),
          },
          {
            title: 'Shiratama Anmitsu',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '15.50',
            image: require('../../../assets/png/menus/menu2.png'),
            image_lg: require('../../../assets/png/menus/menu2.png'),
            image_xl: require('../../../assets/png/menus/menu2.png'),
          },
        ],
      },
    ],
  },
  {
    id: 2,
    title: 'Sean’s Kitchen',
    type: 'American',
    store_status: 'Open Now',
    star_rating: 3.3,
    store_location: '4830 Christiansen Locks Suite 690',
    image: require('../../../assets/png/menus/menu2.png'),
    image_lg: require('../../../assets/png/menus/menu1.png'),
    image_xl: require('../../../assets/png/menus/menu1.png'),
    addons: [
      {
        name: 'Appetizers',
        items: [
          {
            title: 'Beef Tataki',
            description:
              'This slices of rare wagyu beef fillet serve with red onions, spring onions and garlic sauce',
            price: '13.00',
            image: require('../../../assets/png/menus/menu1.png'),
            image_lg: require('../../../assets/png/menus/menu1.png'),
            image_xl: require('../../../assets/png/menus/menu1.png'),
          },
          {
            title: 'Karaage',
            description:
              'Juicy bite size pieces of Japanese style deep fried chicken with a sweet vinegar sauce',
            price: '8.00',
            image: require('../../../assets/png/menus/menu2.png'),
            image_lg: require('../../../assets/png/menus/menu2.png'),
            image_xl: require('../../../assets/png/menus/menu2.png'),
          },
        ],
      },
      {
        name: 'Dessert',
        items: [
          {
            title: 'Yuzu Cheesecake',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '6.00',
            image: require('../../../assets/png/menus/menu1.png'),
            image_lg: require('../../../assets/png/menus/menu1.png'),
            image_xl: require('../../../assets/png/menus/menu1.png'),
          },
          {
            title: 'Shiratama Anmitsu',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '15.50',
            image: require('../../../assets/png/menus/menu2.png'),
            image_lg: require('../../../assets/png/menus/menu2.png'),
            image_xl: require('../../../assets/png/menus/menu2.png'),
          },
        ],
      },
    ],
  },
  {
    id: 3,
    title: 'Using Chow Restaurant',
    type: 'Chinese',
    store_status: 'Open Now',
    star_rating: 3.3,
    store_location: '4830 Christiansen Locks Suite 690',
    image: require('../../../assets/png/menus/menu3.png'),
    image_lg: require('../../../assets/png/menus/menu3.png'),
    image_xl: require('../../../assets/png/menus/menu3.png'),
    addons: [
      {
        name: 'Appetizers',
        items: [
          {
            title: 'Beef Tataki',
            description:
              'This slices of rare wagyu beef fillet serve with red onions, spring onions and garlic sauce',
            price: '13.00',
            image: require('../../../assets/png/menus/menu3.png'),
            image_lg: require('../../../assets/png/menus/menu3.png'),
            image_xl: require('../../../assets/png/menus/menu3.png'),
          },
          {
            title: 'Karaage',
            description:
              'Juicy bite size pieces of Japanese style deep fried chicken with a sweet vinegar sauce',
            price: '8.00',
            image: require('../../../assets/png/menus/menu3.png'),
            image_lg: require('../../../assets/png/menus/menu3.png'),
            image_xl: require('../../../assets/png/menus/menu3.png'),
          },
        ],
      },
      {
        name: 'Dessert',
        items: [
          {
            title: 'Yuzu Cheesecake',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '6.00',
            image: require('../../../assets/png/menus/menu3.png'),
            image_lg: require('../../../assets/png/menus/menu3.png'),
            image_xl: require('../../../assets/png/menus/menu3.png'),
          },
          {
            title: 'Shiratama Anmitsu',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '15.50',
            image: require('../../../assets/png/menus/menu2.png'),
            image_lg: require('../../../assets/png/menus/menu3.png'),
            image_xl: require('../../../assets/png/menus/menu3.png'),
          },
        ],
      },
    ],
  },
  {
    id: 4,
    title: 'Eggless',
    type: 'British',
    store_status: 'Open Now',
    star_rating: 3.3,
    store_location: '4830 Christiansen Locks Suite 690',
    image: require('../../../assets/png/menus/menu4.png'),
    image_lg: require('../../../assets/png/menus/menu4.png'),
    image_xl: require('../../../assets/png/menus/menu4.png'),
    addons: [
      {
        name: 'Appetizers',
        items: [
          {
            title: 'Beef Tataki',
            description:
              'This slices of rare wagyu beef fillet serve with red onions, spring onions and garlic sauce',
            price: '13.00',
            image: require('../../../assets/png/menus/menu4.png'),
            image_lg: require('../../../assets/png/menus/menu4.png'),
            image_xl: require('../../../assets/png/menus/menu4.png'),
          },
          {
            title: 'Karaage',
            description:
              'Juicy bite size pieces of Japanese style deep fried chicken with a sweet vinegar sauce',
            price: '8.00',
            image: require('../../../assets/png/menus/menu4.png'),
            image_lg: require('../../../assets/png/menus/menu4.png'),
            image_xl: require('../../../assets/png/menus/menu4.png'),
          },
        ],
      },
      {
        name: 'Dessert',
        items: [
          {
            title: 'Yuzu Cheesecake',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '6.00',
            image: require('../../../assets/png/menus/menu4.png'),
            image_lg: require('../../../assets/png/menus/menu4.png'),
            image_xl: require('../../../assets/png/menus/menu4.png'),
          },
          {
            title: 'Shiratama Anmitsu',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '15.50',
            image: require('../../../assets/png/menus/menu4.png'),
            image_lg: require('../../../assets/png/menus/menu4.png'),
            image_xl: require('../../../assets/png/menus/menu4.png'),
          },
        ],
      },
    ],
  },
  {
    id: 5,
    title: 'Vietnam Restaurant',
    type: 'Vietnamese',
    store_status: 'Open Now',
    star_rating: 3.3,
    store_location: '4830 Christiansen Locks Suite 690',
    image: require('../../../assets/png/menus/menu5.png'),
    image_lg: require('../../../assets/png/menus/menu5.png'),
    image_xl: require('../../../assets/png/menus/menu5.png'),
    addons: [
      {
        name: 'Appetizers',
        items: [
          {
            title: 'Beef Tataki',
            description:
              'This slices of rare wagyu beef fillet serve with red onions, spring onions and garlic sauce',
            price: '13.00',
            image: require('../../../assets/png/menus/menu5.png'),
            image_lg: require('../../../assets/png/menus/menu5.png'),
            image_xl: require('../../../assets/png/menus/menu5.png'),
          },
          {
            title: 'Karaage',
            description:
              'Juicy bite size pieces of Japanese style deep fried chicken with a sweet vinegar sauce',
            price: '8.00',
            image: require('../../../assets/png/menus/menu5.png'),
            image_lg: require('../../../assets/png/menus/menu5.png'),
            image_xl: require('../../../assets/png/menus/menu5.png'),
          },
        ],
      },
      {
        name: 'Dessert',
        items: [
          {
            title: 'Yuzu Cheesecake',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '6.00',
            image: require('../../../assets/png/menus/menu5.png'),
            image_lg: require('../../../assets/png/menus/menu5.png'),
            image_xl: require('../../../assets/png/menus/menu5.png'),
          },
          {
            title: 'Shiratama Anmitsu',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '15.50',
            image: require('../../../assets/png/menus/menu5.png'),
            image_lg: require('../../../assets/png/menus/menu5.png'),
            image_xl: require('../../../assets/png/menus/menu5.png'),
          },
        ],
      },
    ],
  },
  {
    id: 6,
    title: 'Steven Ter Horst Chocolatier',
    type: 'Australian',
    store_status: 'Closed',
    star_rating: 3.3,
    store_location: '4830 Christiansen Locks Suite 690',
    image: require('../../../assets/png/menus/menu6.png'),
    image_lg: require('../../../assets/png/menus/menu6.png'),
    image_xl: require('../../../assets/png/menus/menu6.png'),
    addons: [
      {
        name: 'Appetizers',
        items: [
          {
            title: 'Beef Tataki',
            description:
              'This slices of rare wagyu beef fillet serve with red onions, spring onions and garlic sauce',
            price: '13.00',
            image: require('../../../assets/png/menus/menu6.png'),
            image_lg: require('../../../assets/png/menus/menu6.png'),
            image_xl: require('../../../assets/png/menus/menu6.png'),
          },
          {
            title: 'Karaage',
            description:
              'Juicy bite size pieces of Japanese style deep fried chicken with a sweet vinegar sauce',
            price: '8.00',
            image: require('../../../assets/png/menus/menu6.png'),
            image_lg: require('../../../assets/png/menus/menu6.png'),
            image_xl: require('../../../assets/png/menus/menu6.png'),
          },
        ],
      },
      {
        name: 'Dessert',
        items: [
          {
            title: 'Yuzu Cheesecake',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '6.00',
            image: require('../../../assets/png/menus/menu6.png'),
            image_lg: require('../../../assets/png/menus/menu6.png'),
            image_xl: require('../../../assets/png/menus/menu6.png'),
          },
          {
            title: 'Shiratama Anmitsu',
            description:
              'Japanese style baked cheesecake flavoured with “Yuze” citrus',
            price: '15.50',
            image: require('../../../assets/png/menus/menu6.png'),
            image_lg: require('../../../assets/png/menus/menu6.png'),
            image_xl: require('../../../assets/png/menus/menu6.png'),
          },
        ],
      },
    ],
  },
];
