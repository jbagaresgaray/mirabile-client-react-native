import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TabNotificationScreen from '../modules/Main/TabNotification/TabNotification';

const Stack = createStackNavigator();

const NotificationNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="TabNotification"
      headerMode="none"
      screenOptions={{
        gestureEnabled: false,
        headerBackTitle: '',
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen
        name="TabNotification"
        component={TabNotificationScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default NotificationNavigator;
