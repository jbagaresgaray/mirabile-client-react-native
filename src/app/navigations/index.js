import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const Stack = createStackNavigator();
const RootStack = createStackNavigator();

import AppNavigator from './tab.navigator';

import LoginScreen from '../modules/Auth/Login/Login';
import ForgotPasswordScreen from '../modules/Auth/ForgotPassword/ForgotPassword';
import RegisterScreen from '../modules/Auth/Register/Register';
import VerifyScreen from '../modules/Auth/Verify/Verify';
import GetStartedScreen from '../modules/Auth/GetStarted/GetStarted';
import ResetPasswordScreen from '../modules/Auth/Reset/Reset';
import SearchLocationModalScreen from '../shared/components/SearchLocationModal/SearchLocationModal';

const AuthNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackTitle: '',
        headerBackTitleVisible: false,
        gestureEnabled: false,
      }}>
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ForgotPassword"
        component={ForgotPasswordScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Register"
        component={RegisterScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Verify"
        component={VerifyScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="GetStarted"
        component={GetStartedScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Reset"
        component={ResetPasswordScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Main"
        component={AppNavigator}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const RootStackNavigators = () => {
  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <RootStack.Navigator
          mode="modal"
          headerMode="none"
          initialRouteName="AuthNavigator"
          screenOptions={{
            cardStyle: {backgroundColor: 'rgba(0,0,0,0.4)'},
          }}>
          <RootStack.Screen
            name="AuthNavigator"
            component={AuthNavigator}
            options={{
              headerShown: false,
            }}
          />
          <RootStack.Screen
            name="SearchLocationModal"
            component={SearchLocationModalScreen}
            options={{
              headerShown: false,
            }}
          />
        </RootStack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default RootStackNavigators;
