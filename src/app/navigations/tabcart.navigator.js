import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TabCartScreen from '../modules/Main/TabCart/TabCart';

const Stack = createStackNavigator();

const CartNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="TabCart"
      headerMode="none"
      screenOptions={{
        gestureEnabled: false,
        headerBackTitle: '',
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen
        name="TabCart"
        component={TabCartScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default CartNavigator;
