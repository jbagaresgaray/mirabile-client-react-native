import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';

const Tab = createBottomTabNavigator();
const RootStack = createStackNavigator();

import IconFoodSVG from '../../assets/svg/icon-food.svg';
import IconFoodActiveSVG from '../../assets/svg/icon-food-active.svg';

import IconCartSVG from '../../assets/svg/icon-cart.svg';
import IconCartActiveSVG from '../../assets/svg/icon-cart-active.svg';

import IconNotificationSVG from '../../assets/svg/icon-notification.svg';
import IconNotificationActiveSVG from '../../assets/svg/icon-notification-active.svg';

import IconProfileSVG from '../../assets/svg/icon-profile.svg';
import IconProfileActiveSVG from '../../assets/svg/icon-profile-active.svg';

import ExploreFoodNavigator from './tabexplorefood.navigator';
import CartNavigator from './tabcart.navigator';
import NotificationNavigator from './tabnotification.navigator';
import ProfileNavigator from './tabprofile.navigator';

import CameraScreen from '../shared/components/Camera/Camera';
import DatepickerModal from '../shared/components/Datepicker/Datepicker';
import PickerModal from '../shared/components/PickerModal/PickerModal';
import ContentModalScreen from '../shared/components/ContentModal/ContentModal';
import SearchLocationModalScreen from '../shared/components/SearchLocationModal/SearchLocationModal';

const TabAppNavigator = () => {
  return (
    <SafeAreaProvider>
      <Tab.Navigator
        initialRouteName="YourWorldNavigator"
        tabBarOptions={{
          style: {
            paddingTop: 18,
          },
        }}
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            if (route.name === 'ExploreFoodNavigator') {
              if (focused) {
                return <IconFoodActiveSVG />;
              } else {
                return <IconFoodSVG />;
              }
            } else if (route.name === 'CartNavigator') {
              if (focused) {
                return <IconCartActiveSVG />;
              } else {
                return <IconCartSVG />;
              }
            } else if (route.name === 'NotificationNavigator') {
              if (focused) {
                return <IconNotificationActiveSVG />;
              } else {
                return <IconNotificationSVG />;
              }
            } else if (route.name === 'ProfileNavigator') {
              if (focused) {
                return <IconProfileActiveSVG />;
              } else {
                return <IconProfileSVG />;
              }
            }
          },
        })}>
        <Tab.Screen
          name="ExploreFoodNavigator"
          component={ExploreFoodNavigator}
          options={{
            tabBarLabel: '',
            title: 'Explore Food',
            tabBarAccessibilityLabel: 'Your World',
          }}
        />
        <Tab.Screen
          name="CartNavigator"
          component={CartNavigator}
          options={{
            tabBarLabel: '',
            title: 'Cart',
            tabBarAccessibilityLabel: 'Your World',
          }}
        />
        <Tab.Screen
          name="NotificationNavigator"
          component={NotificationNavigator}
          options={{
            tabBarLabel: '',
            title: 'Notification',
            tabBarAccessibilityLabel: 'Your World',
          }}
        />
        <Tab.Screen
          name="ProfileNavigator"
          component={ProfileNavigator}
          options={{
            tabBarLabel: '',
            title: 'Profile',
            tabBarAccessibilityLabel: 'Your World',
          }}
        />
      </Tab.Navigator>
    </SafeAreaProvider>
  );
};

const RootStackNavigators = () => {
  return (
    <RootStack.Navigator
      mode="modal"
      headerMode="none"
      screenOptions={{
        cardStyle: {backgroundColor: 'rgba(0,0,0,0.4)'},
      }}>
      <RootStack.Screen
        name="TabAppNavigator"
        component={TabAppNavigator}
        options={{
          headerShown: false,
        }}
      />
      <RootStack.Screen
        name="Camera"
        component={CameraScreen}
        options={{
          headerShown: false,
        }}
      />
      <RootStack.Screen
        name="Datepicker"
        component={DatepickerModal}
        options={{
          headerShown: false,
        }}
      />
      <RootStack.Screen
        name="PickerModal"
        component={PickerModal}
        options={{
          headerShown: false,
        }}
      />
      <RootStack.Screen
        name="ContentModal"
        component={ContentModalScreen}
        options={{
          headerShown: false,
        }}
      />
      <RootStack.Screen
        name="SearchLocationModal"
        component={SearchLocationModalScreen}
        options={{
          headerShown: false,
        }}
      />
    </RootStack.Navigator>
  );
};

export default RootStackNavigators;
