import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import TabExploreFoodScreen from '../modules/Main/TabExploreFood/TabExploreFood';

const Stack = createStackNavigator();

const ExploreFoodNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="TabExploreFood"
      headerMode="none"
      screenOptions={{
        gestureEnabled: false,
        headerBackTitle: '',
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen
        name="TabExploreFood"
        component={TabExploreFoodScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default ExploreFoodNavigator;
