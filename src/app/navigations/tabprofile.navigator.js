import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import TabProfileScreen from '../modules/Main/TabProfile/TabProfile';
import AccountInformationScreen from '../modules/Main/TabProfile/AccountInformation/AccountInformation';
import AboutScreen from '../modules/Main/TabProfile/About/About';
import ChangePasswordScreen from '../modules/Main/TabProfile/ChangePassword/ChangePassword';

const Stack = createStackNavigator();

const ProfileNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="TabProfile"
      headerMode="none"
      screenOptions={{
        gestureEnabled: false,
        headerBackTitle: '',
        headerBackTitleVisible: false,
      }}>
      <Stack.Screen
        name="TabProfile"
        component={TabProfileScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="AccountInformation"
        component={AccountInformationScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="About"
        component={AboutScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ChangePassword"
        component={ChangePasswordScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default ProfileNavigator;
