import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  Content,
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Form,
  Item,
  Label,
  Input,
} from 'native-base';

import globals from '../../../../styles/Global';
import styles from './Reset.style';

export default class ResetPasswordScreen extends Component {
  render() {
    return (
      <Container>
        <Header transparent style={globals.appContentHeader}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" style={globals.appBackButton} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content style={[globals.appPadding, globals.viewFlex]}>
          <View style={styles.resetView}>
            <Text style={globals.appHeadingText}>
              Success! Sign in now with {'\n'}your new password.
            </Text>
            <Text style={globals.appNoteText}>
              Your password must be at least 8 characters long.
            </Text>
          </View>
          <View style={styles.resetViewFooter}>
            <Form>
              <Item floatingLabel>
                <Label>New Password</Label>
                <Input textContentType="password" />
              </Item>
              <Item floatingLabel>
                <Label>Confirm Password</Label>
                <Input textContentType="password" />
              </Item>
            </Form>
            <View style={styles.resetViewButton}>
              <Button
                block
                style={[globals.appButton, globals.buttonMirabile]}
                onPress={() => this.props.navigation.navigate('Login')}>
                <Text style={globals.appButtonText}> Save Changes </Text>
              </Button>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
