import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  resetView: {
    marginTop: 36,
  },
  resetViewFooter: {
    flex: 1,
    justifyContent: 'flex-end',
    marginTop: 160,
  },
  resetViewButton: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
});
