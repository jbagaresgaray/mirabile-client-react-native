import {StyleSheet} from 'react-native';
import {FONT_REGULAR} from '../../../../styles/Typography';
import Colors from '../../../../styles/Colors';

export default StyleSheet.create({
  welcomeView: {
    marginTop: 80,
    marginBottom: 150,
  },
  welcomeButtonView: {
    marginTop: 20,
  },
  LoginViewFooter: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    marginBottom: 60,
  },
  loginFormView: {
    marginLeft: -15,
  },
  signUpForgotView: {
    marginTop: 30,
    flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  flexStart: {
    flex: 1,
    alignSelf: 'flex-start',
  },
  flexEnd: {
    flex: 1,
    alignSelf: 'flex-end',
  },
  signUpForgotText: {
    fontSize: 13,
    lineHeight: 15,
    letterSpacing: 0,
    color: Colors.colorStandard,
    ...FONT_REGULAR,
  },
  welcomeOrSeparator: {
    marginTop: 30,
    marginBottom: 20,
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewSeparator: {
    height: 1,
    width: 135,
    backgroundColor: Colors.colorSeparator,
  },
  textSeparator: {
    fontSize: 11,
    lineHeight: 13,
    letterSpacing: 0,
    textAlign: 'center',
    textTransform: 'uppercase',
    color: Colors.colorPlaceholder,
    ...FONT_REGULAR,

    marginLeft: 10,
    marginRight: 10,
  },
});
