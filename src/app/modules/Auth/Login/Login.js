import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  Container,
  Content,
  Button,
  Item,
  Label,
  Input,
  Form,
} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';

import globals from '../../../../styles/Global';
import styles from './Login.style';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        <Content
          style={globals.appPadding}
          contentContainerStyle={globals.viewFlex}>
          <View style={styles.welcomeView}>
            <Text style={globals.appHeadingText}>Welcome to Mirabile</Text>
            <Text style={globals.appNoteText}>Login to your account</Text>
          </View>
          <View style={styles.LoginViewFooter}>
            <Form style={styles.loginFormView}>
              <Item floatingLabel>
                <Label>Email Address</Label>
                <Input
                  textContentType="emailAddress"
                  keyboardType="email-address"
                />
              </Item>
              <Item floatingLabel>
                <Label>Password</Label>
                <Input textContentType="password" />
              </Item>
            </Form>
            <View style={styles.welcomeButtonView}>
              <Button
                block
                style={[globals.appButton, globals.buttonMirabile]}
                onPress={() => this.props.navigation.navigate('Main')}>
                <Text style={globals.appButtonText}> Log in </Text>
              </Button>
              <View style={styles.signUpForgotView}>
                <TouchableOpacity
                  style={styles.flexStart}
                  onPress={() => this.props.navigation.navigate('Register')}>
                  <Text style={styles.signUpForgotText}>Sign up Account</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.flexEnd}
                  onPress={() =>
                    this.props.navigation.navigate('ForgotPassword')
                  }>
                  <Text style={styles.signUpForgotText}>Forgot Password</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.welcomeOrSeparator}>
                <View style={styles.viewSeparator} />
                <Text style={styles.textSeparator}>OR</Text>
                <View style={styles.viewSeparator} />
              </View>
              <Button
                block
                bordered
                style={[globals.appButton, globals.buttonBorderedMirabile]}
                onPress={() => this.props.navigation.navigate('Main')}>
                <Text style={globals.appButtonClearOutlineText}>
                  Explore Food
                </Text>
              </Button>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
