import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  Content,
  Container,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Form,
  Item,
  Label,
  Input,
  Footer,
} from 'native-base';

import globals from '../../../../styles/Global';
import styles from './ForgotPassword.style';

export default class ForgotPasswordScreen extends Component {
  render() {
    return (
      <Container>
        <Header transparent style={globals.appContentHeader}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" style={globals.appBackButton} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content
          style={globals.appPadding}
          contentContainerStyle={globals.viewFlex}>
          <View style={styles.forgotView}>
            <Text style={globals.appHeadingText}>
              Forgot password? No {'\n'}problem!
            </Text>
            <Text style={globals.appNoteText}>
              Enter your registered email address below and we will send you
              instructions on how to reset your password.
            </Text>
          </View>
          <View style={styles.forgotViewFooter}>
            <Form>
              <Item floatingLabel>
                <Label>Email Address</Label>
                <Input
                  textContentType="emailAddress"
                  keyboardType="email-address"
                />
              </Item>
            </Form>
            <View style={styles.forgotViewButton}>
              <Button
                block
                style={[globals.appButton, globals.buttonMirabile]}
                onPress={() =>
                  this.props.navigation.navigate('Verify', {
                    action: 'forgot',
                  })
                }>
                <Text style={globals.appButtonText}> Send </Text>
              </Button>
            </View>
            <View style={styles.forgotViewButton}>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate('Login')}>
                <Text style={globals.appButtonTransparentMirabileText2}>
                  Back to login
                </Text>
              </Button>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
