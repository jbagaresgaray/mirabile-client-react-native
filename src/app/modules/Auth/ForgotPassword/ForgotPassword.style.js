import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  forgotView: {
    marginTop: 36,
  },
  forgotViewFooter: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    marginBottom: 40,
  },
  forgotViewButton: {
    marginTop: 20,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
});
