import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
  Content,
  Text,
  View,
  Input,
} from 'native-base';

import globals from '../../../../styles/Global';
import styles from './GetStarted.style';

export default class GetStartedScreen extends Component {
  render() {
    return (
      <Container>
        <Header transparent style={globals.appContentHeader}>
          <Left>
            <Button
              transparent
              style={styles.appContentHeader}
              onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" style={globals.appBackButton} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content style={globals.appPadding}>
          <View style={styles.getStartedView}>
            <Text style={globals.appHeadingText}>Let’s get started</Text>
            <Text style={globals.appNoteText}>
              To provide you with a better experience, {' \n'} please enter your
              phone number and set up your location.
            </Text>
          </View>
          <View style={styles.getStartedEntryView}>
            <View style={styles.getStartedInput1}>
              <Input placeholder={'+00'} style={styles.getStartedInputText} />
            </View>
            <View style={styles.getStartedInput2}>
              <Input style={styles.getStartedInputText} />
            </View>
          </View>
          <View style={styles.getStartedTermsView}>
            <View style={styles.getStartedButtonView}>
              <Button
                block
                bordered
                style={[globals.appButtonSmall, globals.buttonBorderedMirabile]}
                onPress={() =>
                  this.props.navigation.navigate('SearchLocationModal')
                }>
                <Text style={globals.appButtonClearOutlineText}>
                  Set your location manually
                </Text>
              </Button>
            </View>
            <View style={styles.getStartedTermsView}>
              <Text style={styles.getStartedTermText}>
                We access your location to improve your browsing {'\n'}
                experience
              </Text>
            </View>
          </View>
          <View style={styles.getStartedViewFooter}>
            <View style={styles.getStartedButtonView}>
              <Button
                block
                style={[globals.appButton, globals.buttonMirabile]}
                onPress={() => this.props.navigation.navigate('Main')}>
                <Text style={globals.appButtonText}> Complete </Text>
              </Button>
            </View>
            <View style={styles.getStartedButtonView}>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate('Main')}>
                <Text style={globals.appButtonTransparentText}>
                  Skip this part
                </Text>
              </Button>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
