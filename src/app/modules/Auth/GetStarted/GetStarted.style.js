import {StyleSheet} from 'react-native';
import {FONT_MEDIUM, FONT_REGULAR} from '../../../../styles/Typography';
import Colors from '../../../../styles/Colors';

export default StyleSheet.create({
  getStartedView: {
    marginTop: 35,
  },
  getStartedEntryView: {
    marginTop: 130,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  getStartedInput1: {
    width: 55,
    borderBottomWidth: 2,
    borderBottomColor: '#e0e0e0',
    marginRight: 20,
    textAlign: 'center',
  },
  getStartedInput2: {
    flex: 1,
    borderBottomWidth: 2,
    borderBottomColor: '#e0e0e0',
    textAlign: 'center',
  },
  getStartedInputText: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    color: Colors.colorSeparatorText,
    ...FONT_MEDIUM,
  },
  getStartedTermsView: {
    marginTop: 30,
    height: '100%',
    flex: 1,
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    textAlign: 'center',
  },
  getStartedButtonView: {
    marginTop: 20,
    alignItems: 'center',
  },
  getStartedTermText: {
    fontSize: 11,
    lineHeight: 13,
    letterSpacing: 0,
    color: Colors.colorPlaceholderObject,
    textAlign: 'center',
    ...FONT_REGULAR,
  },
  getStartedViewFooter: {
    marginTop: 86,
    paddingBottom: 20,
  },
});
