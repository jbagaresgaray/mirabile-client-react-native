import {StyleSheet} from 'react-native';
import {FONT_MEDIUM, FONT_REGULAR} from '../../../../styles/Typography';
import Colors from '../../../../styles/Colors';

export default StyleSheet.create({
  verifyView: {
    marginTop: 36,
  },
  verifyEntryView: {
    marginTop: 130,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  verifyInput: {
    width: 35,
    borderBottomColor: '#e0e0e0',
    borderBottomWidth: 2,
    marginRight: 20,
    textAlign: 'center',
  },
  verifyInputText: {
    fontSize: 32,
    lineHeight: 38,
    letterSpacing: 0,
    color: Colors.colorSeparator,
    textAlign: 'center',
    ...FONT_MEDIUM,
  },
  verifyButtonView: {
    marginTop: 35,
  },
  verifyTermsView: {
    marginBottom: 35,
  },
  verifyTermsText: {
    fontSize: 13,
    lineHeight: 15,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandardNote,
    ...FONT_REGULAR,
  },
});
