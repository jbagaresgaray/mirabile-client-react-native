import React, {Component} from 'react';
import {Text, View} from 'react-native';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';

import globals from '../../../../styles/Global';
import styles from './Verify.style';
import {
  Container,
  Content,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Right,
} from 'native-base';

export default class VerifyScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
    };
  }

  render() {
    const {route} = this.props;
    const {action} = route.params;
    console.log('action: ', action);

    let labelHeader = '';
    if (action === 'register') {
      labelHeader = 'Verify identity';
    } else if (action === 'forgot') {
      labelHeader = 'Check your email';
    }
    return (
      <Container>
        <Header transparent style={globals.appContentHeader}>
          <Left>
            <Button
              transparent
              style={styles.appContentHeader}
              onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" style={globals.appBackButton} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content style={globals.appPadding}>
          <View style={styles.verifyView}>
            <Text style={globals.appHeadingText}>{labelHeader}</Text>
            <Text style={globals.appNoteText}>
              Please enter the 6-digit code sent to (email address) to confirm
              email and activate Mirabile account.
            </Text>
          </View>
          <View style={styles.verifyEntryView}>
            {/* <View style={styles.verifyInput}>
              <Text style={styles.verifyInputText}>0</Text>
            </View>
            <View style={styles.verifyInput}>
              <Text style={styles.verifyInputText}>0</Text>
            </View>
            <View style={styles.verifyInput}>
              <Text style={styles.verifyInputText}>0</Text>
            </View>
            <View style={styles.verifyInput}>
              <Text style={styles.verifyInputText}>0</Text>
            </View>
            <View style={styles.verifyInput}>
              <Text style={styles.verifyInputText}>0</Text>
            </View>
            <View style={styles.verifyInput}>
              <Text style={styles.verifyInputText}>0</Text>
            </View> */}

            <SmoothPinCodeInput
              ref={this.pinInput}
              value={this.state.code}
              cellStyle={styles.verifyInput}
              cellStyleFocused={styles.verifyInput}
              textSize={styles.verifyInputText}
              cellSize={44}
              codeLength={6}
              onTextChange={code => this.setState({code})}
              onFulfill={this._checkCode}
              onBackspace={this._focusePrevInput}
            />
          </View>
          <View style={styles.verifyButtonView}>
            <View style={styles.verifyTermsView}>
              <Text style={styles.verifyTermText}>
                Didn’t receive a code?{' '}
                <Text style={globals.appLink}>Resend code</Text>
              </Text>
            </View>
            <Button
              block
              style={[globals.appButton, globals.buttonMirabile]}
              onPress={() => {
                if (action === 'register') {
                  this.props.navigation.navigate('GetStarted');
                } else if (action === 'forgot') {
                  this.props.navigation.navigate('Reset');
                }
              }}>
              <Text style={globals.appButtonText}> Verify & Proceed </Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
