import {StyleSheet} from 'react-native';
import {FONT_REGULAR} from '../../../../styles/Typography';
import Colors from '../../../../styles/Colors';

export default StyleSheet.create({
  registerView: {
    marginTop: 36,
    marginBottom: 150,
  },
  registerViewFooter: {
    // position: 'absolute',
    // bottom: 0,
    // left: 0,
    // right: 0,
    // paddingRight: 30,
    // paddingLeft: 30,
    paddingBottom: 40,
  },
  registerForm: {
    marginLeft: -15,
    marginBottom: 30,
  },
  registerTermsText: {
    fontSize: 13,
    lineHeight: 15,
    letterSpacing: 0,
    color: Colors.colorStandard,
    textAlign: 'left',
    ...FONT_REGULAR,
  },
  registerButtonView: {
    marginTop: 30,
  },
  registerTermsView: {
    marginBottom: 20,
  },
  registerSignInView: {
    marginTop: 30,
  },
});
