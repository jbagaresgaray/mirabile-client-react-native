import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  Container,
  Content,
  Button,
  Item,
  Label,
  Input,
  Form,
  Left,
  Right,
  Body,
  Header,
  Icon,
} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';

import globals from '../../../../styles/Global';
import styles from './Register.style';

export default class RegisterScreen extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Container>
        <Header transparent style={globals.appContentHeader}>
          <Left>
            <Button
              transparent
              style={styles.appContentHeader}
              onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" style={globals.appBackButton} />
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content style={globals.appPadding}>
          <View style={styles.registerView}>
            <Text style={globals.appHeadingText}>New to Mirabile?</Text>
            <Text style={globals.appNoteText}>Sign up your new account</Text>
          </View>
          <View style={styles.registerViewFooter}>
            <Form style={styles.registerForm}>
              <Item floatingLabel>
                <Label>Email Address</Label>
                <Input
                  textContentType="emailAddress"
                  keyboardType="email-address"
                  placeholderTextColor={'#AFAFAF'}
                />
              </Item>
              <Item floatingLabel>
                <Label>Password</Label>
                <Input
                  secureTextEntry={true}
                  textContentType="password"
                  placeholderTextColor={'#AFAFAF'}
                />
              </Item>
              <Item floatingLabel>
                <Label>Confirm Password</Label>
                <Input
                  secureTextEntry={true}
                  textContentType="password"
                  placeholderTextColor={'#AFAFAF'}
                />
              </Item>
            </Form>
            <View style={styles.registerButtonView}>
              <View style={styles.registerTermsView}>
                <Text style={styles.registerTermsText}>
                  By creating an account, you agree to our{' '}
                  <Text style={globals.appLink}>Privacy & Terms.</Text>
                </Text>
              </View>
              <Button
                block
                style={[globals.appButton, globals.buttonMirabile]}
                onPress={() =>
                  this.props.navigation.navigate('Verify', {
                    action: 'register',
                  })
                }>
                <Text style={globals.appButtonText}> Sign Up </Text>
              </Button>
              <View style={styles.registerSignInView}>
                <Text style={styles.registerTermsText}>
                  Already have an account?{' '}
                  <Text
                    style={globals.appLink}
                    onPress={() => {
                      this.props.navigation.navigate('Login');
                    }}>
                    Login.
                  </Text>
                </Text>
              </View>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}
