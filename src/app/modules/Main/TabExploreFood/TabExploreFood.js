import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  Container,
  Content,
  List,
  ListItem,
  Body,
  Left,
  Thumbnail,
} from 'native-base';

import globals from '../../../../styles/Global';
import styles from './TabExploreFood.style';
import {MENUS} from '../../../shared/constants/menus';
import {CATEGORIES} from '../../../shared/constants/categories';
import MenuItem from '../../../shared/components/MenuItem/MenuItem';

export default class TabExploreFoodScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menusArr: MENUS,
      categoriesArr: CATEGORIES,
    };
  }

  render() {
    return (
      <Container>
        <Content style={globals.appPaddingModal}>
          <List>
            {this.state.menusArr.map((menu, key) => {
              return (
                <MenuItem
                  image={menu.image}
                  title={menu.title}
                  type={menu.type}
                  store_location={menu.store_location}
                />
              );
            })}
          </List>
        </Content>
      </Container>
    );
  }
}
