import React, {Component} from 'react';
import {Text, StyleSheet, Dimensions} from 'react-native';
import {List, ListItem, Body, Right, View, Left} from 'native-base';
import HTML from 'react-native-render-html';
import ContentLoader from 'react-native-easy-content-loader';

import {FONT_REGULAR} from '../../../../../styles/Typography';
import Colors from '../../../../../styles/Colors';
import {TimeAgo} from '../../../../shared/components/TimeAgo/TimeAgo';
import NotificationStatus from '../../../../shared/components/NotificationStatus/NotificationStatus';

const styles = StyleSheet.create({
  notificationView: {
    marginTop: 15,
  },
  notificationList: {
    marginLeft: -15,
  },
  notificationItem: {
    position: 'relative',
    marginBottom: 10,
    borderBottomWidth: 0,
    minHeight: 102,
    backgroundColor: Colors.colorWhite,
  },
  unreadNotificationItem: {
    backgroundColor: '#BB271F0B',
  },
  notificationTitle: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    color: Colors.colorStandard,
    ...FONT_REGULAR,
  },
  notificationSubtitle: {
    fontSize: 11,
    lineHeight: 13,
    letterSpacing: 0,
    color: Colors.colorStandardNote,
    ...FONT_REGULAR,
    marginTop: 5,
  },
  notificationNote: {
    fontSize: 11,
    lineHeight: 13,
    letterSpacing: 0,
    color: Colors.colorPlaceholder,
    ...FONT_REGULAR,
  },
  flex1: {
    flex: 0.1,
    position: 'relative',
  },
  flex2: {
    flex: 0.2,
    position: 'relative',
  },
  notificationStatus: {
    position: 'absolute',
    top: -20,
    paddingLeft: 15,
  },
});

export default class NotificationListComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <ContentLoader
          active
          containerStyles={styles.notificationView}
          pRows={2}
          listSize={10}
          loading={this.props.isLoading}>
          <List style={styles.notificationList}>
            {this.props.dataArr &&
              this.props.dataArr.map((item, key) => {
                return (
                  <ListItem
                    key={key}
                    style={[
                      styles.notificationItem,
                      item.isUnread ? styles.unreadNotificationItem : null,
                    ]}>
                    <Left style={styles.flex1}>
                      <View style={styles.notificationStatus}>
                        {item.isUnread && <NotificationStatus />}
                      </View>
                    </Left>
                    <Body>
                      {/* <Text style={styles.notificationTitle}>
                    {item.title}
                  </Text> */}
                      <HTML
                        html={item.title}
                        imagesMaxWidth={Dimensions.get('window').width}
                      />
                      <Text style={styles.notificationSubtitle}>
                        {item.subtitle}
                      </Text>
                    </Body>
                    <Right style={styles.flex2}>
                      <View>
                        <TimeAgo
                          style={styles.notificationNote}
                          time={item.created_at}
                        />
                      </View>
                    </Right>
                  </ListItem>
                );
              })}
          </List>
        </ContentLoader>
      </>
    );
  }
}
