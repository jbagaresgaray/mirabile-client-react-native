import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  notificationView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerMiddleView: {
    justifyContent: 'center',
    flex: 1,
  },
  refreshControlView: {
    backgroundColor: '#E0FFFF',
  },
});
