import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {FONT_MEDIUM, FONT_REGULAR} from '../../../../../styles/Typography';
import Colors from '../../../../../styles/Colors';

import globals from '../../../../../styles/Global';

import NoNotificationSVG from '../../../../../assets/svg/img-no-notification.svg';

const styles = StyleSheet.create({
  notificationView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    padding: 20,
  },
  cardContentView: {
    marginTop: 40,
  },
  cardStatus: {
    fontSize: 14,
    lineHeight: 26,
    letterSpacing: 0,
    color: Colors.colorStandard,
    ...FONT_MEDIUM,

    textAlign: 'center',
    marginTop: 40,
  },
  cardStatusNote: {
    fontSize: 11,
    lineHeight: 13,
    letterSpacing: 0,
    color: Colors.colorStandard,
    ...FONT_REGULAR,

    textAlign: 'center',
    marginTop: 10,
  },
});

export default class NoNotificationComponent extends Component {
  render() {
    return (
      <View style={styles.notificationView}>
        <NoNotificationSVG />
        <View style={styles.cardContentView}>
          <Text style={styles.cardStatus}>No new notifications</Text>
          <Text style={styles.cardStatusNote}>
            Explore the menu again and place your order in seconds!
          </Text>
        </View>
      </View>
    );
  }
}
