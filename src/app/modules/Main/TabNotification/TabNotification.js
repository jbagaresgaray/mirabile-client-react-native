import React, {Component} from 'react';
import {View} from 'react-native';
import {
  Container,
  Content,
  Header,
  Left,
  Body,
  Right,
  Button,
  Text,
  Title,
} from 'native-base';
import {RefreshControl} from 'react-native';
import isEmpty from 'lodash/isEmpty';
import globals from '../../../../styles/Global';
import styles from './TabNotification.style';

import NoNotificationComponent from './NoNotification/NoNotification';
import NotificationListComponent from './NotificationList/NotificationList';

export default class TabNotificationScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showContent: true,
      fakeArr: Array.from({length: 12}),
      notificationArr: [
        {
          title: 'Wasaiss Japanese Kitchen <b>Confirmed</b> your orders.',
          subtitle:
            'Restaurant is preparing your food and wait for them to notify you once your food is ready.',
          created_at: new Date(),
          isUnread: true,
        },
        {
          title: 'Sean’s Kitchen <b>Cancelled</b> your orders.',
          subtitle:
            'Your order has been cancelled due to a ingredients shortage.',
          created_at: new Date(),
          isUnread: true,
        },
        {
          title: 'Sean’s Kitchen <b>Cancelled</b> your orders.',
          subtitle:
            'Your order has been cancelled due to a ingredients shortage.',
          created_at: new Date(),
          isUnread: false,
        },
        {
          title: 'Your orders from <b>Eggless</b> encountered some errors.',
          subtitle:
            'You can order again by checking your orders or browse again their menu.',
          created_at: new Date(),
          isUnread: true,
        },
      ],
    };
  }

  componentDidMount() {
    this.onRefresh();
  }

  clearNotifications() {
    this.setState({
      notificationArr: [],
    });
  }

  onRefresh() {
    console.log('onRefresh:');
    setTimeout(() => {
      this.setState({
        showContent: false,
      });
    }, 2000);
  }

  renderRefreshControl() {
    return (
      <RefreshControl
        style={styles.refreshControlView}
        refreshing={!this.state.showContent}
        onRefresh={this.onRefresh}
        tintColor="#ff0000"
        title="Loading..."
        titleColor="#00ff00"
        colors={['#ff0000', '#00ff00', '#0000ff']}
        progressBackgroundColor="#ffff00"
      />
    );
  }

  render() {
    return (
      <Container>
        <Header transparent style={globals.appContentHeader}>
          <Left />
          <Body>
            <Title style={globals.appTitle}>Notifications</Title>
          </Body>
          <Right>
            <Button transparent onPress={() => this.clearNotifications()}>
              <Text style={globals.appButtonClearOutlineText}>Clear</Text>
            </Button>
          </Right>
        </Header>
        <Content
          style={globals.viewContent2}
          contentContainerStyle={
            isEmpty(this.state.notificationArr) ? styles.centerMiddleView : null
          }
          refreshControl={this.renderRefreshControl}>
          {isEmpty(this.state.notificationArr) ? (
            <View style={styles.notificationView}>
              <NoNotificationComponent />
            </View>
          ) : (
            <NotificationListComponent
              isLoading={this.state.showContent}
              dataArr={this.state.notificationArr}
            />
          )}
        </Content>
      </Container>
    );
  }
}
