import {StyleSheet} from 'react-native';
import Colors from '../../../../../styles/Colors';
import {FONT_REGULAR} from '../../../../../styles/Typography';

export default StyleSheet.create({
  transparentList: {
    marginLeft: -15,
  },
  listItemAccount: {
    height: 50,
    backgroundColor: Colors.colorWhite,
  },
  listItemAccountLabel: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandard,
    ...FONT_REGULAR,

    marginLeft: 15,
  },
  listItemAccountNote: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorPlaceholder,
    ...FONT_REGULAR,
  },
  footerView: {
    margin: 6,
  },
});
