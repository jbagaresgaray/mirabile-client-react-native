import React, {Component} from 'react';
import {
  Container,
  Button,
  Text,
  Content,
  Right,
  Title,
  Body,
  Left,
  Header,
  Icon,
  Form,
  Item,
  Label,
  Input,
  Footer,
  FooterTab,
} from 'native-base';
import globals from '../../../../../styles/Global';
import styles from './ChangePassword.style';

import CustomToast from '../../../../shared/components/RootToastCT/CustomToast';
import Toast from '../../../../shared/components/RootToastCT/Toast';

export default class ChangePasswordScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showSuccessToast: false,
    };
  }

  showToast() {
    this.setState({
      showSuccessToast: true,
    });

    setTimeout(
      () =>
        this.setState({
          showSuccessToast: false,
        }),
      2000,
    );
  }

  onSavePassword() {
    this.showToast();
  }

  render() {
    return (
      <>
        <CustomToast
          visible={this.state.showSuccessToast}
          iconType="Ionicons"
          icon="md-checkmark"
          message={'Password has been changed'}
        />
        <Container>
          <Header>
            <Left style={globals.noFlex}>
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}>
                <Icon name="ios-arrow-back" style={globals.appBackButton} />
              </Button>
            </Left>
            <Body style={globals.viewFlex}>
              <Title style={globals.appTitle}>Change Password</Title>
            </Body>
            <Right style={globals.noFlex}>
              <Button transparent onPress={() => this.onSavePassword()}>
                <Text style={globals.appButtonClearOutlineText}>Save</Text>
              </Button>
            </Right>
          </Header>
          <Content>
            <Form style={styles.transparentList}>
              <Item fixedLabel style={styles.listItemAccount}>
                <Label style={[styles.listItemAccountLabel, globals.viewFlex]}>
                  New Password
                </Label>
                <Input
                  style={styles.listItemAccountNote}
                  secureTextEntry={true}
                  textContentType="password"
                />
              </Item>
              <Item fixedLabel style={styles.listItemAccount}>
                <Label style={[styles.listItemAccountLabel, globals.viewFlex]}>
                  Confirm Password
                </Label>
                <Input
                  style={styles.listItemAccountNote}
                  secureTextEntry={true}
                  textContentType="password"
                />
              </Item>
            </Form>
          </Content>
          <Footer style={styles.footerView}>
            <FooterTab>
              <Button
                block
                style={[globals.appButton, globals.buttonMirabile]}
                onPress={() => this.onSavePassword()}>
                <Text style={globals.appButtonText}> Save </Text>
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      </>
    );
  }
}
