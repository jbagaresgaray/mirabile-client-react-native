import {StyleSheet} from 'react-native';
import Colors from '../../../../styles/Colors';
import {FONT_REGULAR} from '../../../../styles/Typography';

export default StyleSheet.create({
  profileViewContainer: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 60,
    marginBottom: 40,
  },
  profileView: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'yellow',
  },
  profileViewImage: {
    borderRadius: 100 / 2,
  },
  profileViewImageContainer: {
    width: 90,
    height: 90,

    borderRadius: 100 / 2,
    borderWidth: 4,
    borderStyle: 'solid',
    borderColor: Colors.colorWhite,
  },
  profileCameraIconView: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',

    position: 'absolute',
    right: -8,
    top: 10,
    zIndex: 1,
    backgroundColor: Colors.colorWhite,

    width: 28,
    height: 28,
    borderRadius: 100 / 2,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: Colors.colorMirabile,
  },
  profileViewName: {
    fontSize: 18,
    lineHeight: 21,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandard,
    ...FONT_REGULAR,

    marginTop: 10,
  },
  profileList: {
    backgroundColor: Colors.colorWhite,
    paddingLeft: 10,
  },
  listItemProfile: {
    height: 50,
  },
  listItemProfileLabel: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandard,
    ...FONT_REGULAR,
  },
});
