import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  Container,
  Content,
  List,
  ListItem,
  Left,
  Body,
  ActionSheet,
} from 'native-base';
import {Avatar} from 'react-native-elements';

import globals from '../../../../styles/Global';
import styles from './TabProfile.style';

import IconAccountInfoSVG from '../../../../assets/svg/icon-account-info.svg';
import IconPaymentMethodSVG from '../../../../assets/svg/icon-payment-method.svg';
import IconCustomerSupportSVG from '../../../../assets/svg/icon-customer-support.svg';
import IconInfoSVG from '../../../../assets/svg/icon-info.svg';
import IconLogoutSVG from '../../../../assets/svg/icon-logout.svg';
import CameraIconSVG from '../../../../assets/svg/icon-camera.svg';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default class TabProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  ACTIONSHEET_BUTTONS = ['Take a photo', 'Select from album', 'Cancel'];
  CANCEL_INDEX = 4;

  onCameraClick() {
    ActionSheet.show(
      {
        options: this.ACTIONSHEET_BUTTONS,
        cancelButtonIndex: this.CANCEL_INDEX,
      },
      buttonIndex => {
        this.setState({clicked: this.ACTIONSHEET_BUTTONS[buttonIndex]});
        if (buttonIndex === 0) {
          this.onCameraPhoto();
        } else if (buttonIndex === 1) {
          this.onCameraAlbum();
        }
      },
    );
  }

  onCameraPhoto() {
    this.props.navigation.navigate('Camera');
  }

  onCameraAlbum() {
    this.props.navigation.navigate('Camera');
  }

  render() {
    const defaultImg = require('../../../../assets/png/no-avatar/no-avatar.png');

    return (
      <Container>
        <Content style={[globals.viewContent]}>
          <View style={styles.profileViewContainer}>
            <View style={styles.profileView}>
              <View>
                <View style={styles.profileCameraIconView}>
                  <TouchableOpacity onPress={() => this.onCameraClick()}>
                    <CameraIconSVG />
                  </TouchableOpacity>
                </View>
                <Avatar
                  rounded
                  source={defaultImg}
                  avatarStyle={styles.profileViewImage}
                  containerStyle={styles.profileViewImageContainer}
                />
              </View>
              <View>
                <Text style={styles.profileViewName}>George Hayes</Text>
              </View>
            </View>
          </View>
          <List style={styles.profileList}>
            <ListItem
              icon
              last
              style={styles.listItemProfile}
              onPress={() =>
                this.props.navigation.navigate('AccountInformation')
              }>
              <Left>
                <IconAccountInfoSVG />
              </Left>
              <Body>
                <Text style={styles.listItemProfileLabel}>
                  Account Information
                </Text>
              </Body>
            </ListItem>
            {/* <ListItem icon last style={styles.listItemProfile}>
              <Left>
                <IconPaymentMethodSVG />
              </Left>
              <Body>
                <Text style={styles.listItemProfileLabel}>Payment Method</Text>
              </Body>
            </ListItem> */}
            <ListItem icon last style={styles.listItemProfile}>
              <Left>
                <IconCustomerSupportSVG />
              </Left>
              <Body>
                <Text style={styles.listItemProfileLabel}>
                  Customer Support
                </Text>
              </Body>
            </ListItem>
            <ListItem
              icon
              last
              style={styles.listItemProfile}
              onPress={() => this.props.navigation.navigate('About')}>
              <Left>
                <IconInfoSVG />
              </Left>
              <Body>
                <Text style={styles.listItemProfileLabel}>About Mirabile</Text>
              </Body>
            </ListItem>
            <ListItem
              icon
              last
              style={styles.listItemProfile}
              onPress={() => this.props.navigation.navigate('Login')}>
              <Left>
                <IconLogoutSVG />
              </Left>
              <Body>
                <Text style={styles.listItemProfileLabel}>Logout</Text>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
