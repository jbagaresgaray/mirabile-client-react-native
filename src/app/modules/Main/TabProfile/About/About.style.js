import {StyleSheet} from 'react-native';
import {FONT_REGULAR} from '../../../../../styles/Typography';
import Colors from '../../../../../styles/Colors';

export default StyleSheet.create({
  listAbout: {
    backgroundColor: Colors.colorWhite,
  },
  listItemAbout: {
    height: 50,
  },
  listItemAboutLabel: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandard,
    ...FONT_REGULAR,
  },
  listItemAboutNote: {
    fontSize: 12,
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorSeparatorText,
    ...FONT_REGULAR,
  },
  listItemAboutIcon: {
    fontSize: 21,
  },
  listItemVersionInfo: {
    marginTop: 12,
  },
});
