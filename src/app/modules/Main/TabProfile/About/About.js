import React, {Component} from 'react';
import {Text, View, Platform} from 'react-native';
import {
  Container,
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Right,
  Content,
  List,
  ListItem,
} from 'native-base';
import DeviceInfo from 'react-native-device-info';

import styles from './About.style';
import globals from '../../../../../styles/Global';

export default class AboutScreen extends Component {
  getBaseOS() {
    if (Platform.OS === 'ios') {
      return 'IOS';
    } else if (Platform.OS === 'android') {
      return 'Android';
    }
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" style={globals.appBackButton} />
            </Button>
          </Left>
          <Body>
            <Title style={globals.appTitle}>About Mirabile</Title>
          </Body>
          <Right />
        </Header>
        <Content style={globals.viewContent}>
          <List style={styles.listAbout}>
            <ListItem
              style={styles.listItemAbout}
              onPress={() =>
                this.props.navigation.navigate('ContentModal', {
                  title: 'Privacy Policy',
                  type: 'web',
                  content: 'https://reactnative.dev/docs/getting-started',
                })
              }>
              <Text style={styles.listItemProfileLabel}>Privacy Policy</Text>
            </ListItem>
            <ListItem
              style={styles.listItemAbout}
              onPress={() =>
                this.props.navigation.navigate('ContentModal', {
                  title: 'Terms of Use',
                  type: 'web',
                  content: 'https://reactnative.dev/docs/components-and-apis',
                })
              }>
              <Text style={styles.listItemProfileLabel}>Terms of Use</Text>
            </ListItem>
            <ListItem itemDivider />
            <ListItem note>
              <Body>
                <Text style={styles.listItemProfileLabel}>Build Version</Text>
                <View style={styles.listItemVersionInfo}>
                  <View>
                    <Text note style={styles.listItemAboutNote}>
                      Mirabile for {this.getBaseOS()}{' '}
                      {DeviceInfo.getReadableVersion()}
                    </Text>
                  </View>
                  <View>
                    <Text note style={styles.listItemAboutNote}>
                      ©2020 Mirabile, Inc. All right reserved.
                    </Text>
                  </View>
                </View>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
