import {StyleSheet} from 'react-native';
import {FONT_REGULAR} from '../../../../../styles/Typography';
import Colors from '../../../../../styles/Colors';

export default StyleSheet.create({
  listItemAccount: {
    height: 50,
    backgroundColor: Colors.colorWhite,
  },
  listItemAccountLabel: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandard,
    ...FONT_REGULAR,

    marginLeft: 15,
  },
  listItemAccountNote: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorPlaceholder,
    ...FONT_REGULAR,
  },
  listItemAccountLabelPlaceholder: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorPlaceholder,
    ...FONT_REGULAR,
  },
  footerView: {
    margin: 6,
  },
  transparentList: {
    marginLeft: -15,
  },
  spacerView: {
    paddingTop: 20,
  },
});
