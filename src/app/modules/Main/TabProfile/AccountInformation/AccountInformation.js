import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {
  Content,
  Container,
  Header,
  Right,
  Left,
  Button,
  Icon,
  Body,
  Title,
  List,
  ListItem,
  Form,
  Item,
  Label,
  Input,
  Footer,
  FooterTab,
} from 'native-base';
import {TouchableOpacity} from 'react-native-gesture-handler';

import globals from '../../../../../styles/Global';
import styles from './AccountInformation.style';
import CustomToast from '../../../../shared/components/RootToastCT/CustomToast';

export default class AccountInformationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUpdate: false,
      showToast: false,
    };
  }

  onEditClick() {
    this.setState({
      isUpdate: true,
    });
  }

  onCancelClick() {
    this.setState({
      isUpdate: false,
    });
  }

  onSaveClick() {
    this.setState({
      isUpdate: false,
      showToast: true,
    });

    setTimeout(() => {
      this.setState({
        showToast: false,
      });
    }, 2000);
  }

  onGenderClick() {
    this.props.navigation.navigate('PickerModal', {
      pickerData: ['Male', 'Female'],
    });
  }

  onBirthdayClick() {
    this.props.navigation.navigate('Datepicker');
  }

  onViewScreen() {
    return (
      <List style={styles.transparentList}>
        <ListItem style={styles.listItemAccount}>
          <Left>
            <Text style={styles.listItemAccountLabel}>Full Name</Text>
          </Left>
          <Right style={globals.viewFlex}>
            <TouchableOpacity onPress={() => this.onEditClick()}>
              <Text style={styles.listItemAccountNote}>George Hayes</Text>
            </TouchableOpacity>
          </Right>
        </ListItem>
        <ListItem style={styles.listItemAccount}>
          <Left>
            <Text style={styles.listItemAccountLabel}>Email Address</Text>
          </Left>
          <Right style={globals.viewFlex}>
            <TouchableOpacity onPress={() => this.onEditClick()}>
              <Text style={styles.listItemAccountNote}>george@email.com</Text>
            </TouchableOpacity>
          </Right>
        </ListItem>
        <ListItem style={styles.listItemAccount}>
          <Left>
            <Text style={styles.listItemAccountLabel}>Password</Text>
          </Left>
          <Right style={globals.viewFlex}>
            <TouchableOpacity onPress={() => this.onEditClick()}>
              <Text />
            </TouchableOpacity>
          </Right>
        </ListItem>
        <ListItem style={styles.listItemAccount}>
          <Left>
            <Text style={styles.listItemAccountLabel}>Phone</Text>
          </Left>
          <Right style={globals.viewFlex}>
            <TouchableOpacity onPress={() => this.onEditClick()}>
              <Text style={styles.listItemAccountNote}>+11 111 1111 111</Text>
            </TouchableOpacity>
          </Right>
        </ListItem>
        <ListItem style={styles.listItemAccount}>
          <Left>
            <Text style={styles.listItemAccountLabel}>Gender</Text>
          </Left>
          <Right style={globals.viewFlex}>
            <TouchableOpacity onPress={() => this.onEditClick()}>
              <Text style={styles.listItemAccountNote}>Female</Text>
            </TouchableOpacity>
          </Right>
        </ListItem>
        <ListItem style={styles.listItemAccount}>
          <Left>
            <Text style={styles.listItemAccountLabel}>Birthday</Text>
          </Left>
          <Right style={globals.viewFlex}>
            <TouchableOpacity onPress={() => this.onEditClick()}>
              <Text style={styles.listItemAccountNote}>1986-09-20</Text>
            </TouchableOpacity>
          </Right>
        </ListItem>
      </List>
    );
  }

  onEditScreen() {
    return (
      <Form style={styles.transparentList}>
        <Item fixedLabel last style={styles.listItemAccount}>
          <Label style={[styles.listItemAccountLabel, globals.viewFlex]}>
            Full Name
          </Label>
          <Input style={styles.listItemAccountNote} textContentType="name" />
        </Item>
        <Item fixedLabel last style={styles.listItemAccount}>
          <Label style={[styles.listItemAccountLabel, globals.viewFlex]}>
            Email Address
          </Label>
          <Input
            style={styles.listItemAccountNote}
            textContentType="emailAddress"
            keyboardType="email-address"
          />
        </Item>
        <Item fixedLabel last style={styles.listItemAccount}>
          <Label style={[styles.listItemAccountLabel, globals.viewFlex]}>
            Password
          </Label>
          <Input
            style={styles.listItemAccountNote}
            secureTextEntry={true}
            textContentType="password"
          />
        </Item>
        <Item fixedLabel last style={styles.listItemAccount}>
          <Label style={[styles.listItemAccountLabel, globals.viewFlex]}>
            Phone
          </Label>
          <Input
            style={styles.listItemAccountNote}
            textContentType="telephoneNumber"
            keyboardType="phone-pad"
          />
        </Item>
        <ListItem style={styles.listItemAccount}>
          <Left>
            <Text style={styles.listItemAccountLabel}>Gender</Text>
          </Left>
          <Body style={globals.viewFlex}>
            <TouchableOpacity onPress={() => this.onGenderClick()}>
              <Text style={styles.listItemAccountNote}>Female</Text>
            </TouchableOpacity>
          </Body>
        </ListItem>
        <ListItem style={styles.listItemAccount}>
          <Left>
            <Text style={styles.listItemAccountLabel}>Birthday</Text>
          </Left>
          <Body style={globals.viewFlex}>
            <TouchableOpacity onPress={() => this.onBirthdayClick()}>
              <Text style={styles.listItemAccountNote}>1986-09-20</Text>
            </TouchableOpacity>
          </Body>
        </ListItem>
      </Form>
    );
  }

  render() {
    return (
      <>
        <CustomToast
          visible={this.state.showToast}
          iconType="Ionicons"
          icon="md-checkmark"
          message={`Your profile has been \n updated`}
        />
        <Container>
          <Header
            style={[
              globals.colorContent,
              globals.noHeaderBorder,
              globals.appHeader,
            ]}>
            <Left style={globals.viewFlex5}>
              {!this.state.isUpdate ? (
                <>
                  <Button
                    transparent
                    onPress={() => this.props.navigation.goBack()}>
                    <Icon name="ios-arrow-back" style={globals.appBackButton} />
                  </Button>
                </>
              ) : (
                <>
                  <Button transparent onPress={() => this.onCancelClick()}>
                    <Text style={globals.appHeaderClearButtonText}>Cancel</Text>
                  </Button>
                </>
              )}
            </Left>
            <Body>
              <Title style={globals.appTitle}>Account Information</Title>
            </Body>
            <Right style={globals.viewFlex5}>
              {this.state.isUpdate && (
                <>
                  <Button transparent onPress={() => this.onSaveClick()}>
                    <Text style={globals.appButtonClearOutlineText}>Save</Text>
                  </Button>
                </>
              )}
            </Right>
          </Header>
          <Content
            style={globals.viewContent}
            contentContainerStyle={globals.viewFlex}>
            {!this.state.isUpdate ? this.onViewScreen() : this.onEditScreen()}
            <View style={[styles.spacerView, styles.transparentList]}>
              <ListItem
                style={styles.listItemAccount}
                onPress={() =>
                  this.props.navigation.navigate('ChangePassword')
                }>
                <Text style={styles.listItemAccountLabel}>Change Password</Text>
              </ListItem>
            </View>
          </Content>
          {this.state.isUpdate && (
            <Footer style={styles.footerView}>
              <FooterTab>
                <Button
                  block
                  style={[globals.appButton, globals.buttonMirabile]}
                  onPress={() => this.onSaveClick}>
                  <Text style={globals.appButtonText}> Save </Text>
                </Button>
              </FooterTab>
            </Footer>
          )}
        </Container>
      </>
    );
  }
}
