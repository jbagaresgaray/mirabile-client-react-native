import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {Container, Content} from 'native-base';

import globals from '../../../../styles/Global';
import styles from './TabCart.style';


export default class TabCartScreen extends Component {
  render() {
    return (
      <Container>
        <Content style={globals.appPadding}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text>Cart</Text>
          </View>
        </Content>
      </Container>
    );
  }
}
