import {StyleSheet} from 'react-native';
import {Colors} from './Variables';
import {FONT_REGULAR, FONT_MEDIUM, FONT_BOLD} from './Typography';

export default StyleSheet.create({
  viewFlex: {
    flex: 1,
    // backgroundColor: 'yellow',
  },
  viewFlex5: {
    flex: 0.5,
  },
  noFlex: {
    flex: 0,
  },
  viewPage: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: 'flex',
    position: 'absolute',
    flexDirection: 'column',
    justifyContent: 'space-between',
    overflow: 'hidden',
    zIndex: 0,
  },
  viewContent: {
    backgroundColor: Colors.colorContent,
  },
  viewContent2: {
    backgroundColor: Colors.colorContent2,
  },
  buttonMirabile: {
    backgroundColor: Colors.colorMirabile,
  },
  buttonBorderedMirabile: {
    borderWidth: 1,
    borderColor: Colors.colorMirabile,
  },
  noHeaderBorder: {
    borderBottomWidth: 0,
    marginBottom: 5,
  },
  noFooterBorder: {
    borderTopWidth: 0,
  },
  appPadding: {
    paddingStart: 30,
    paddingEnd: 30,
  },
  appPaddingModal: {
    paddingStart: 15,
    paddingEnd: 15,
  },
  appButton: {
    borderRadius: 6,
    height: 48,
  },
  appButtonSmall: {
    height: 38,
  },
  appButtonText: {
    fontSize: 16,
    lineHeight: 19,
    letterSpacing: 0,
    color: Colors.colorWhite,
    ...FONT_REGULAR,
  },
  appButtonClearOutlineText: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    color: Colors.colorMirabile,
    ...FONT_REGULAR,
  },
  appButtonTransparentText: {
    fontSize: 13,
    lineHeight: 15,
    letterSpacing: 0,
    color: Colors.colorStandardNote,
    textAlign: 'center',
    ...FONT_BOLD,
  },
  appButtonTransparentMirabileText: {
    fontSize: 13,
    lineHeight: 15,
    letterSpacing: 0,
    color: Colors.colorMirabile,
    textAlign: 'center',
    ...FONT_BOLD,
  },
  appButtonTransparentMirabileText2: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    color: Colors.colorMirabile,
    textAlign: 'center',
    ...FONT_MEDIUM,
  },
  appBackButton: {
    color: Colors.colorStandardNote,
    fontSize: 20,
  },
  appHeader: {
    marginLeft: 15,
  },
  appContentHeader: {
    paddingLeft: 25,
  },
  appTitle: {
    fontSize: 18,
    lineHeight: 21,
    letterSpacing: 0,
    textAlign: 'center',
    color: Colors.colorStandard,
    ...FONT_REGULAR,
  },
  appHeaderClearButtonText: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorPlaceholder,
    ...FONT_REGULAR,
  },
  appModalTitle: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandard,
    ...FONT_MEDIUM,
  },
  appHeadingText: {
    fontSize: 24,
    lineHeight: 28,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandard,
    ...FONT_MEDIUM,
  },
  appNoteText: {
    fontSize: 16,
    lineHeight: 19,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorStandardNote,
    ...FONT_REGULAR,
    marginTop: 15,
  },
  appLink: {
    fontSize: 13,
    lineHeight: 15,
    letterSpacing: 0,
    textAlign: 'left',
    color: Colors.colorMirabile,
    ...FONT_MEDIUM,
  },
  labelFloating: {
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0,
    color: Colors.colorPlaceholder,
    ...FONT_REGULAR,
  },
});
